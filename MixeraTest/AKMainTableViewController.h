//
//  AKMainTableViewController.h
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKMainTableViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) UIRefreshControl *refreshControl;
@property (assign,nonatomic) BOOL userAllows;

@end
