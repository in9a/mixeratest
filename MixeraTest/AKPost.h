//
//  AKPost.h
//  MixeraTest
//
//  Created by Антон on 03.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AKPost : NSObject

@property(strong,nonatomic) NSString *type;
@property(strong,nonatomic) NSString *mark;
@property(strong,nonatomic) NSString *colour;
@property(strong,nonatomic) NSString *condition;
@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *phone;
@property(strong,nonatomic) NSURL *urlImage;
@property(strong,nonatomic) UIImage *image;

@end
