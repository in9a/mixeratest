//
//  AKMainTableViewCell.h
//  MixeraTest
//
//  Created by Антон on 03.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKMainTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageMain;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *colourLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;


@end
