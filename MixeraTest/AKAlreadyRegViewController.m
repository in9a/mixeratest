//
//  AKAlreadyRegViewController.m
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import "AKAlreadyRegViewController.h"
#import "AKAlertController.h"
#import "AKDataMeneger.h"
#import "AKUser.h"
#import "AKMainTableViewController.h"

@interface AKAlreadyRegViewController ()

@end

@implementation AKAlreadyRegViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)tapAction:(UITapGestureRecognizer*)sender {
    
    [self.view endEditing:YES];
}


- (IBAction)enterAction:(UIButton *)sender {

    //*** All registered user are saved to core data (only as test example)
   
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:kentityUser
                                              inManagedObjectContext:[AKDataMeneger sharedManager].managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(email = %@) && (password = %@)", _emailFild.text, _passwordField.text];

    [fetchRequest setPredicate:predicate];
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                   ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [[AKDataMeneger sharedManager].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count==0) {
        AKAlertController*alert=[AKAlertController sharedAlert];
        [alert alertCreateWhithName:@"Ошибка" andMessage:@"Проверьте еще раз" andController:self];
    } else {
        //NSLog(@"%@", [fetchedObjects description]);
        
        AKMainTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AKMainTableViewController"];
   
        vc.userAllows = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
    }

}



@end
