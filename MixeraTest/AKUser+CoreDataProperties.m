//
//  AKUser+CoreDataProperties.m
//  MixeraTest
//
//  Created by Антон on 04.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AKUser+CoreDataProperties.h"

@implementation AKUser (CoreDataProperties)

@dynamic name;
@dynamic phone;
@dynamic email;
@dynamic password;

@end
