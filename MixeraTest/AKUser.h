//
//  AKUser.h
//  MixeraTest
//
//  Created by Антон on 04.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AKUser : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "AKUser+CoreDataProperties.h"
