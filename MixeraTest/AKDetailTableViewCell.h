//
//  AKDetailTableViewCell.h
//  MixeraTest
//
//  Created by Антон on 04.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
