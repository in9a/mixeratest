//
//  AKDataMeneger.h
//  42CoreData
//
//  Created by Антон on 26.10.15.
//  Copyright © 2015 Антон. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AKUser;

static NSString*kentityUser=@"AKUser";

@interface AKDataMeneger : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (AKDataMeneger*) sharedManager;

-(AKUser*)addUserwhithName:(NSString*)username andPhone:(NSString*)phone andEmail:(NSString*)email andPass:(NSString*)passw;
-(NSArray*) allObjects;

- (void) printAllObjects;
- (void) deleteAllObjects;
- (void)saveContext;

@end
