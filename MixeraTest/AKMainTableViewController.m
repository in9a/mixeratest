//
//  AKMainTableViewController.m
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import "AKMainTableViewController.h"
#import "AKMainTableViewCell.h"
#import "AKPost.h"
#import <AFNetworking/AFNetworking.h>
#import "AKDetailTableViewCell.h"
#import "AKAlertController.h"
#import <VK-ios-sdk/VKSdk.h>


@interface AKMainTableViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *objectsArray;
    NSMutableArray *postArray;
    NSMutableArray *postImage;
    NSMutableArray *boolForRows;
    AKMainTableViewCell* header;
    
}

@end

@implementation AKMainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    //***refresh controller for json parsing (if it's not local)
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    
    [self.refreshControl addTarget:self
                       action:@selector(getLatestJson)
             forControlEvents:UIControlEventValueChanged];
    
    NSString* message = [NSString stringWithFormat:@"Обновление"];
    
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:message
                                                                     attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:11.0]}];
    [self.tableView addSubview: self.refreshControl];

    //***
    
    [self readJsonFromPath];

    
    // *** set all rows to be cloased
    boolForRows=[[NSMutableArray alloc]init];
    
    if (postArray.count>0) {
        for (int i = 0; i<=[postArray count];i++) {
            [boolForRows addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    //***
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAction:)];
    self.navigationItem.rightBarButtonItem = right;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
- (void) logoutAction:(id) sender {
    
    [VKSdk forceLogout];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}
- (void)getLatestJson{
    
    // ***closing all sections
    for (int i = 0; i<=[postArray count];i++) {
        [boolForRows replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
    }

    // ***gettingNewJson
    [self readJsonFromPath];
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void) readJsonFromPath {
    
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"listExample" ofType:@"json"];
    
    NSError * error = nil;
    NSData * dataJson = [NSData dataWithContentsOfFile:filePath];
    
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:dataJson
                                                             options:NSJSONReadingMutableContainers
                                                               error:&error];
    if (error) {
        //NSLog(@"Json response error %@", [error localizedDescription]);
       
        AKAlertController*alert=[AKAlertController sharedAlert];
        [alert alertCreateWhithName:@"Json!" andMessage:[NSString stringWithFormat:@"%@",[error localizedDescription]] andController:self];

    } else {
        
        objectsArray = [jsonDict objectForKey:@"response"];
    
        postArray = [NSMutableArray array];
        postImage = [NSMutableArray array];
        
        
        for (NSArray* obj in objectsArray) {
            
            AKPost* post = [[AKPost alloc]init];
            
            if ([obj valueForKey:@"type"]) {
                post.type = [obj valueForKey:@"type"];
            }
            if ([obj valueForKey:@"mark"]) {
                post.mark = [obj valueForKey:@"mark"];
            }
            if ([obj valueForKey:@"colour"]) {
                post.colour = [obj valueForKey:@"colour"];
            }
            if (![[obj valueForKey:@"condition"] isEqualToString:@""]) {
                post.condition = [obj valueForKey:@"condition"];
            } else {
                post.condition = @"n/a";
            }
            if (![[obj valueForKey:@"name"] isEqualToString:@""]) {
                post.name = [obj valueForKey:@"name"];
            } else {
                post.name = @"Имя не указано";
            }
            if (![[obj valueForKey:@"phone"]isEqualToString:@""]) {
                post.phone = [obj valueForKey:@"phone"];
            } else {
                post.phone = @"";
            }
            if (![[obj valueForKey:@"url"] isEqualToString:@""]) {
                NSString *stringURL = [obj valueForKey:@"url"];
                NSURL *url = [NSURL URLWithString:stringURL];
                post.urlImage = url;
            }
            else {
                post.urlImage = nil;
            }
            [postArray addObject:post];
        
            
            for (AKPost * post in postArray) {
                
                if (post.urlImage != nil) {
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *task = [session dataTaskWithURL:post.urlImage completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                        if (!error) {
                            
                            post.image = [UIImage imageWithData:data];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.tableView reloadData];
                                
                            });
                        }
                        else {
                            //NSLog(@"Errror %@",[error localizedDescription]);
                            post.image = [UIImage imageNamed:@"car.png"];
//                            [self.tableView reloadData];
                        }
                    }];
                    [task resume];
                } else {
                    post.image = [UIImage imageNamed:@"car.png"];
                    
                }
            }
        }
    
        

        
    }
}

#pragma mark - TableView
#pragma mark - HEADER

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 81.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [postArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *ident = @"HeaderCell";
    
    AKMainTableViewCell* headerCell = [tableView dequeueReusableCellWithIdentifier:ident];
    
    if (!headerCell) {
        headerCell = [[AKMainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident];
    }

    AKPost * post = [postArray objectAtIndex:section];
    headerCell.nameLabel.text = post.mark;
    headerCell.typeLabel.text = post.type;
    headerCell.conditionLabel.text = post.condition;
    headerCell.colourLabel.text = post.colour;
    
    if (post.image == nil) {
        [headerCell.activityView startAnimating];
    } else {
        [headerCell.activityView stopAnimating];
    }
    headerCell.imageMain.image = post.image;

    //*** CHECK FOR USER RIGHTS
    
    
    if (self.userAllows == YES) {
     
    //*** Creating a View to add a tapGesture whith open action
        
        UIView *view = headerCell.contentView;
        
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        view.tag = section;
    
        [view addGestureRecognizer:headerTapped];
        
        return view;
    } else {
            
        headerCell.detailImage.image = nil;
        
//        UIView *view = headerCell.contentView;
        
        return headerCell;
       
    }

}

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)sender{
    
//    //NSLog(@"%ld",sender.view.tag);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.view.tag];
   
//    //NSLog(@"%d", indexPath.section);
    
    BOOL chekForRows  = [[boolForRows objectAtIndex:indexPath.section] boolValue];

    for (int i=0; i<[postArray count]; i++) {
        if (indexPath.section==i) {
            [boolForRows replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!chekForRows]];
            
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.view.tag] withRowAnimation:UITableViewRowAnimationFade];
        } else {
            
            if ( [[boolForRows objectAtIndex:i] isEqualToNumber:[NSNumber numberWithBool:YES]]){
                
                [boolForRows replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:i] withRowAnimation:UITableViewRowAnimationFade];
            };
        }
    }
}

#pragma mark - ROW

-(void) tableView:(UITableView *) tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //    //row number on which you want to animate your view
    //    //row number could be either 0 or 1 as you are creating two cells
    //    //suppose you want to animate view on cell at 0 index
    //    if(indexPath.row == 0) //check for the 0th index cell
    //    {
    //        // access the view which you want to animate from it's tag
    //        UIView *myView = [cell.contentView viewWithTag:MY_VIEW_TAG];
    //
    //        // apply animation on the accessed view
    //        [UIView animateWithDuration:1.2
    //                              delay:0
    //                            options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut animations:^
    //         {
    //             [myView setAlpha:0.0];
    //         } completion:^(BOOL finished)
    //         {
    //             [myView setAlpha:1.0];
    //         }];
    //    }
    
    
    
    //    BOOL chekForRows  = [[boolForRows objectAtIndex:section] boolValue];
    //    if (chekForRows) {
    //
    //
    //
    //
    //        headerCell.detailImage.image = [UIImage imageNamed:@"carat-open.png"];
    //        //        [headerCell.detailButton setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateNormal];
    //        [headerCell.detailButton setSelected:YES];
    //    } else {
    //        //    [headerCell.detailButton setBackgroundImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
    //
    //
    //        [headerCell.detailButton setBackgroundImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
    //        headerCell.detailImage.image = [UIImage imageNamed:@"carat.png"];
    //    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[boolForRows objectAtIndex:indexPath.section] boolValue]) {
        return 50;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if ([[boolForRows objectAtIndex:section] boolValue]) {
        return 1;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString * ident=@"Cell";
    
    AKDetailTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:ident];
    
    if (!cell) {
        cell=[[AKDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ident];
    }
    
    
    BOOL openCell  = [[boolForRows objectAtIndex:indexPath.section] boolValue];
    
    //***Closed
    if(!openCell) {
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.text=@"";
    }
    //***Opened
    else {
        AKPost *post = [postArray objectAtIndex:indexPath.section];
        
        cell.nameLabel.text=post.name;
        cell.phoneLabel.text = post.phone;
        cell.callButton.tag = indexPath.section;
        [cell.callButton addTarget:self action:@selector(buttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void) buttonTouched:(UIButton*)sender {
    AKPost *post = [postArray objectAtIndex:sender.tag];
    
    if (![post.phone isEqualToString:@""]) {
        NSString *phoneNumber = post.phone;
        NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
   //NSLog(@"didEndDisplayingCell %ld",(long)indexPath.row);

    BOOL chekForRows  = [[boolForRows objectAtIndex:indexPath.section] boolValue];
    
    if (chekForRows) {
        [boolForRows replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:!chekForRows]];
    }

}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
 
            for (NSIndexPath * path in self.tableView.indexPathsForVisibleRows) {

                    AKMainTableViewCell * cell = [self.tableView cellForRowAtIndexPath:path];
                
                    if (cell.layer.position.y > 30) {
                        
                        //NSLog(@"delete");
                        [boolForRows replaceObjectAtIndex:path.section withObject:[NSNumber numberWithBool:NO]];
                    
                        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:path.section] withRowAnimation:UITableViewRowAnimationFade];
                    }
            }
}

@end
