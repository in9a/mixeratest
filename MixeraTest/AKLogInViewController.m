//
//  AKLogInViewController.m
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import "AKLogInViewController.h"
#import "AKMainTableViewController.h"
#import "AKDataMeneger.h"
#import "AKUser.h"
#import "AKAlertController.h"
#import <VK-ios-sdk/VKSdk.h>

static NSString *segueReg = @"nonreg";
static NSArray *SCOPE = nil;

@interface AKLogInViewController () <VKSdkUIDelegate, VKSdkDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)dismissKeyboard:(UITextField *)sender;

@end


@implementation AKLogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SCOPE = @[VK_PER_WALL];

    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"5274080"];
    
    [sdkInstance registerDelegate:self];
   
    [sdkInstance setUiDelegate:self];
 
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (!error) {
            
            if (state == VKAuthorizationAuthorized) {
                [self pushWithAutorized];
            }

        } else {
            AKAlertController*alert=[AKAlertController sharedAlert];
            [alert alertCreateWhithName:@"Внимание!" andMessage:[NSString stringWithFormat:@"%@", error] andController:self];
        }
        
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)viewDidDisappear:(BOOL)animated{
    _nameField.text = nil;
    _emailField.text = nil;
    _phoneField.text = nil;
    _passwordField.text = nil;
}

#pragma mark - Actions

//VK



- (IBAction)vkAutorization:(UIButton *)sender {
    
    [VKSdk authorize:SCOPE];
    
}

- (IBAction)registeredAction:(UIButton *)sender {
    
    [self checkFields];
    
}

#pragma mark - VK
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController presentViewController:controller animated:YES completion:nil];
    
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [self vkAutorization:nil];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self pushWithAutorized];
    } else if (result.error) {
        
        AKAlertController*alert=[AKAlertController sharedAlert];
        [alert alertCreateWhithName:@"Внимание!" andMessage:[NSString stringWithFormat:@"Access denied\n%@", result.error]andController:self];
    }
}

- (void)vkSdkUserAuthorizationFailed {
    AKAlertController*alert=[AKAlertController sharedAlert];
    [alert alertCreateWhithName:@"Access denied!" andMessage:@"Access denied!" andController:self];
}


#pragma mark - Registration

-(void) checkFields {
    //***Check for empty fields
    
    if ([_nameField.text isEqualToString:@""]  || [_emailField.text isEqualToString:@""] || [_passwordField.text isEqualToString:@""] || [_passwordField.text length]<=3) {
        
        AKAlertController*alert=[AKAlertController sharedAlert];
        [alert alertCreateWhithName:@"Внимание!" andMessage:@"Заполните обязательные поля формы регистрации" andController:self];
        
    } else {
        //Chek emailField
        if (![self validateEmailWithString:_emailField.text]) {
            AKAlertController*alert=[AKAlertController sharedAlert];
            [alert alertCreateWhithName:@"Внимание!" andMessage:@"Проверьте правильность поля email" andController:self];
        } else {
            [self registerNewUser];
        }
    }

}

-(void) registerNewUser{
    
    AKDataMeneger *manager = [AKDataMeneger sharedManager];
    
    
    //*** saving curent user
    [manager addUserwhithName:_nameField.text andPhone:_phoneField.text andEmail:_emailField.text andPass:_passwordField.text];
    
    [self pushWithAutorized];
    
}

- (void) pushWithAutorized{
    [self.navigationController popToRootViewControllerAnimated:NO];
    AKMainTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AKMainTableViewController"];
    
    vc.userAllows = YES;
    
     [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - noReg

- (IBAction)noRegUser:(UIButton *)sender {
    
    AKMainTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AKMainTableViewController"];
    
    vc.userAllows = NO;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - TextFields

- (void)tapAction:(UITapGestureRecognizer*)sender {
    
  [self.view endEditing:YES];
}

- (IBAction)dismissKeyboard:(UITextField *)sender {
    
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:_nameField]) {
        [_emailField becomeFirstResponder];
    } else if ([textField isEqual:_emailField]) {
        [_phoneField becomeFirstResponder];
    } else if ([textField isEqual:_passwordField]){
        [self checkFields];
        [textField resignFirstResponder];
    } else {
        
    }
    
    return YES;
}
//***phoneField editing

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_phoneField]) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        NSArray* components = [string componentsSeparatedByCharactersInSet:validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];
        
        newString = [validComponents componentsJoinedByString:@""];
        
        static const int localNumberMaxLength = 7;
        static const int areaCodeMaxLength = 3;
        static const int countryCodeMaxLength = 3;
        
        NSMutableString* resultString = [NSMutableString string];
        
        NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);
        
        if (localNumberLength > 0) {
            
            NSString* number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
            
            [resultString appendString:number];
            
            if ([resultString length] > 3) {
                [resultString insertString:@"-" atIndex:3];
            }
            
        }
        
        if ([newString length] > localNumberMaxLength) {
            
            NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
            
            NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
            
            NSString* area = [newString substringWithRange:areaRange];
            
            area = [NSString stringWithFormat:@"(%@) ", area];
            
            [resultString insertString:area atIndex:0];
        }
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
            
            NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
            
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            
            NSString* countryCode = [newString substringWithRange:countryCodeRange];
            
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            
            [resultString insertString:countryCode atIndex:0];
        }
        
        
        textField.text = resultString;
        
        return NO;
    }
    return YES;
}

//***Email Validation

- (BOOL)validateEmailWithString:(NSString*)email {
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    return [emailTest evaluateWithObject:email];
}

 #pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     AKMainTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AKMainTableViewController"];
     

     if ([segue.identifier isEqualToString:segueReg]) {
        
         vc.userAllows = NO;
     } else {
        
         vc.userAllows = YES;
     }

     
 }


@end
