//
//  AKUser+CoreDataProperties.h
//  MixeraTest
//
//  Created by Антон on 04.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AKUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface AKUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *password;

@end

NS_ASSUME_NONNULL_END
