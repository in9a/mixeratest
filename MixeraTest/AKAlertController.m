//
//  AKAlertController.m
//  Lancelot
//
//  Created by Антон on 17.12.15.
//  Copyright © 2015 Антон. All rights reserved.
//

#import "AKAlertController.h"




@implementation AKAlertController

+ (AKAlertController*) sharedAlert {
    
     AKAlertController* alertAlloc = nil;
    
    alertAlloc=[[AKAlertController alloc] init];
        
    return alertAlloc;

}



-(void) alertCreateWhithName :(NSString*)name andMessage :(NSString*) message andController: (UIViewController*) controler{

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:name
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];


    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];

    [alert addAction:ok];

    [controler presentViewController:alert animated:YES completion:nil];
}


@end
