//
//  AKAlreadyRegViewController.h
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKAlreadyRegViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailFild;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)enterAction:(UIButton *)sender;

@end
