//
//  AKAlertController.h
//  Lancelot
//
//  Created by Антон on 17.12.15.
//  Copyright © 2015 Антон. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AKAlertController : NSObject

+ (AKAlertController*) sharedAlert;

-(void) alertCreateWhithName :(NSString*)name andMessage :(NSString*) message andController: (UIViewController*) controler;
@end
