//
//  AppDelegate.h
//  MixeraTest
//
//  Created by Антон on 02.02.16.
//  Copyright © 2016 Антон. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

