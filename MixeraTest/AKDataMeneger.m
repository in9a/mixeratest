//
//  AKDataMeneger.m
//  42CoreData
//
//  Created by Антон on 26.10.15.
//  Copyright © 2015 Антон. All rights reserved.
//

#import "AKDataMeneger.h"
#import <UIKit/UIKit.h>
#import "AKUser.h"


@implementation AKDataMeneger


+ (AKDataMeneger*) sharedManager {
    
    static AKDataMeneger* manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[AKDataMeneger alloc]init];
        
    });
    
    return manager;
}

#pragma mark - ADDAction

//**************

-(AKUser*)addUserwhithName:(NSString*)username andPhone:(NSString*)phone andEmail:(NSString*)email andPass:(NSString*)passw {
    
    AKUser*user = [NSEntityDescription insertNewObjectForEntityForName:kentityUser
                                                  inManagedObjectContext:self.managedObjectContext];
    user.name = username;
    user.phone = phone;
    user.password = passw;
    user.email = email;

    [self.managedObjectContext save:nil];
    
    return user;
}


-(NSArray*) allObjects {
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * desription = [NSEntityDescription entityForName:kentityUser
                                                   inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:desription];
    
    NSError * error = nil;
    
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if (error) {
        //NSLog(@"%@", [error localizedDescription]);
        
    }
    return resultArray;
}

#pragma mark - PRINT
- (void) printArray :(NSArray*) array{

//    for (AKUser* object in array){
//     
//        //NSLog(@"%@ %@ %@",object.name, object.phone, object.password);
//    }
}

- (void) printAllObjects {
    
    NSArray* resultArray = [self allObjects];
    [self printArray:resultArray];
    
}

#pragma mark - Delete
- (void) deleteAllObjects{
   
    NSArray* array = [self allObjects];
    
    for (id object in array){
        
        [self.managedObjectContext deleteObject:object];
        
    }
    [self.managedObjectContext save:nil];
    
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "ANT._2CoreData" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {

    
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MixeraTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
   
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MixeraTest.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];}
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;

}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
